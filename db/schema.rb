# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130808125509) do

  create_table "accounts", :force => true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "usertype"
    t.integer  "logged"
    t.string   "first_name"
    t.string   "last_name"
    t.float    "calltime_normative"
    t.integer  "show_in_realtime_stats"
    t.float    "balance"
    t.float    "frozen_balance"
    t.integer  "lcr_id"
    t.integer  "postpaid"
    t.integer  "blocked"
    t.integer  "tariff_id"
    t.float    "month_plan_perc"
    t.datetime "month_plan_updated"
    t.integer  "sales_this_month"
    t.integer  "sales_this_month_planned"
    t.integer  "show_billing_info"
    t.integer  "primary_device_id"
    t.float    "credit"
    t.string   "clientid"
    t.string   "agreement_number"
    t.date     "agreement_date"
    t.string   "language"
    t.integer  "taxation_country"
    t.string   "vat_number"
    t.float    "vat_percent"
    t.integer  "address_id"
    t.string   "accounting_number"
    t.integer  "owner_id"
    t.integer  "hidden"
    t.integer  "allow_loss_calls"
    t.datetime "vouchers_disabled_till"
    t.string   "uniquehash"
    t.integer  "c2c_service_active"
    t.integer  "temporary_id"
    t.integer  "send_invoice_types"
    t.integer  "call_limit"
    t.float    "c2c_call_price"
    t.integer  "sms_tariff_id"
    t.integer  "sms_lcr_id"
    t.integer  "sms_service_active"
    t.integer  "cyberplat_active"
    t.integer  "call_center_agent"
    t.integer  "generate_invoice"
    t.float    "tax_1"
    t.float    "tax_2"
    t.float    "tax_3"
    t.float    "tax_4"
    t.date     "block_at"
    t.integer  "block_at_conditional"
    t.integer  "block_conditional_use"
    t.integer  "recording_enabled"
    t.integer  "recording_forced_enabled"
    t.string   "recordings_email"
    t.integer  "recording_hdd_quota"
    t.integer  "warning_email_active"
    t.float    "warning_email_balance"
    t.integer  "warning_email_sent"
    t.integer  "tax_id"
    t.integer  "invoice_zero_calls"
    t.integer  "acc_group_id"
    t.integer  "hide_destination_end"
    t.integer  "warning_email_hour"
    t.integer  "warning_balance_call"
    t.integer  "warning_balance_sound_file_id"
    t.integer  "own_providers"
    t.integer  "ignore_global_monitorings"
    t.integer  "currency_id"
    t.integer  "quickforwards_rule_id"
    t.integer  "spy_device_id"
    t.float    "time_zone"
    t.integer  "minimal_charge"
    t.datetime "minimal_charge_start_at"
    t.integer  "webphone_allow_use"
    t.integer  "webphone_device_id"
    t.integer  "responsible_accountant_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "addresses", :force => true do |t|
    t.string   "address_country"
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "address_vat"
    t.string   "address_phone_1"
    t.string   "address_phone_2"
    t.string   "address_fax"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "call_records", :force => true do |t|
    t.integer  "accounting_code"
    t.string   "user_name"
    t.datetime "call_date"
    t.integer  "bill_sec"
    t.float    "user_price"
    t.string   "prefix"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "calls", :force => true do |t|
    t.datetime "calldate"
    t.string   "clid"
    t.string   "src"
    t.string   "dst"
    t.string   "dcontext"
    t.string   "channel"
    t.string   "dstchannel"
    t.string   "lastapp"
    t.string   "lastdata"
    t.integer  "duration"
    t.integer  "billsec"
    t.string   "disposition"
    t.integer  "amaflags"
    t.string   "accountcode"
    t.string   "uniqueid"
    t.string   "userfield"
    t.integer  "src_device_id"
    t.integer  "dst_device_id"
    t.integer  "processed"
    t.float    "did_price"
    t.integer  "card_id"
    t.integer  "provider_id"
    t.float    "provider_rate"
    t.integer  "provider_billsec"
    t.float    "provider_price"
    t.string   "account_id"
    t.float    "user_rate"
    t.integer  "user_billsec"
    t.float    "user_price"
    t.integer  "reseller_id"
    t.float    "reseller_rate"
    t.integer  "reseller_billsec"
    t.float    "reseller_price"
    t.integer  "partner_id"
    t.float    "partner_rate"
    t.integer  "partner_billsec"
    t.float    "partner_price"
    t.string   "prefix"
    t.integer  "server_id"
    t.integer  "hangupcause"
    t.string   "callertype"
    t.string   "peerip"
    t.string   "recvip"
    t.string   "sipfrom"
    t.string   "uri"
    t.string   "useragent"
    t.string   "peername"
    t.integer  "t38passthrough"
    t.float    "did_inc_price"
    t.float    "did_prov_price"
    t.string   "localized_dst"
    t.integer  "did_provider_id"
    t.integer  "did_id"
    t.string   "originator_ip"
    t.string   "terminator_ip"
    t.float    "real_duration"
    t.float    "real_billsec"
    t.integer  "did_billsec"
    t.integer  "dst_user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "margins", :force => true do |t|
    t.datetime "sampled_at"
    t.integer  "time_window"
    t.float    "price"
    t.float    "margin"
    t.integer  "accounting_code"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "user_name"
    t.integer  "call_count"
    t.integer  "duration_sec"
  end

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "reports", :force => true do |t|
    t.float    "margin"
    t.integer  "accounting_code"
    t.integer  "month"
    t.integer  "year"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.float    "price"
    t.string   "user_name"
    t.integer  "call_count"
    t.integer  "duration_sec"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "accounting_code"
    t.float    "margin"
    t.integer  "address_id"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
