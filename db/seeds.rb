# This file should contain all the record creation needed to seed the database with its default values.
# Run rake db:seed (or create alongside the db with db:setup).

puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.find_or_create_by_name({ :name => role }, :without_protection => true)
  puts 'role: ' << role
end

15.upto(17) do |n|
  address = Address.create :address_line_1 => Faker::Address.street_address, :address_country => 'Italy', :address_vat => rand(0..1000)+1000, :address_phone_1 => Faker::PhoneNumber.phone_number
  puts 'created address: ' << n.to_s
  address.save
end


puts 'DEFAULT USERS'
user = User.find_or_create_by_email :name => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup, :accounting_code => 998, :margin => 0, :address_id => 15
puts 'user: ' << user.name
user.add_role :admin
user.skip_confirmation!
user.save!
user = User.find_or_create_by_email :name => ENV['OWNER_NAME'].dup, :email => ENV['OWNER_EMAIL'].dup, :password => ENV['OWNER_PASSWORD'].dup, :password_confirmation => ENV['OWNER_PASSWORD'].dup, :accounting_code => 1, :margin => 90, :address_id => 16
puts 'user: ' << user.name
user.add_role :owner
user.skip_confirmation!
user.save!
user = User.find_or_create_by_email :name => ENV['SUPPORT_NAME'].dup, :email => ENV['SUPPORT_EMAIL'].dup, :password => ENV['SUPPORT_PASSWORD'].dup, :password_confirmation => ENV['SUPPORT_PASSWORD'].dup, :accounting_code => 999, :margin => 0, :address_id => 17
puts 'user: ' << user.name
user.add_role :owner
user.skip_confirmation!
user.save!
user = User.find_or_create_by_email :name => "Sebastiano", :email => "wholesale@helloskt.com", :password => "5tardu5t78", :confirmation_password => "5tardu5t78", :accounting_code => 995, :margin => 0, :address_id => 16
puts 'user: ' << user.name
user.add_role :owner
user.skip_confirmation!
user.save!
