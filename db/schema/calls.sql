-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: 37.59.1.175    Database: mor
-- ------------------------------------------------------
-- Server version	5.5.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calls`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calldate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clid` varchar(80) NOT NULL,
  `src` varchar(80) NOT NULL,
  `dst` varchar(80) NOT NULL,
  `dcontext` varchar(80) NOT NULL,
  `channel` varchar(80) NOT NULL,
  `dstchannel` varchar(80) NOT NULL,
  `lastapp` varchar(80) NOT NULL,
  `lastdata` varchar(80) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `disposition` varchar(45) NOT NULL,
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL,
  `uniqueid` varchar(32) NOT NULL,
  `userfield` varchar(255) NOT NULL,
  `src_device_id` int(11) NOT NULL DEFAULT '0',
  `dst_device_id` int(11) NOT NULL DEFAULT '0',
  `processed` tinyint(4) NOT NULL DEFAULT '0',
  `did_price` double NOT NULL DEFAULT '0',
  `card_id` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `provider_rate` double DEFAULT NULL,
  `provider_billsec` int(11) DEFAULT NULL,
  `provider_price` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_rate` double DEFAULT NULL,
  `user_billsec` int(11) DEFAULT NULL,
  `user_price` double DEFAULT NULL,
  `reseller_id` int(11) DEFAULT NULL,
  `reseller_rate` double DEFAULT NULL,
  `reseller_billsec` int(11) DEFAULT NULL,
  `reseller_price` double DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `partner_rate` double DEFAULT NULL,
  `partner_billsec` int(11) DEFAULT NULL,
  `partner_price` double DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `server_id` int(11) DEFAULT '1',
  `hangupcause` int(11) DEFAULT NULL,
  `callertype` enum('Local','Outside') DEFAULT 'Local',
  `peerip` varchar(255) DEFAULT NULL,
  `recvip` varchar(255) DEFAULT NULL,
  `sipfrom` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  `peername` varchar(255) DEFAULT NULL,
  `t38passthrough` tinyint(4) DEFAULT NULL,
  `did_inc_price` double DEFAULT '0',
  `did_prov_price` double DEFAULT '0',
  `localized_dst` varchar(50) DEFAULT NULL,
  `did_provider_id` int(11) DEFAULT '0',
  `did_id` int(11) DEFAULT NULL,
  `originator_ip` varchar(20) DEFAULT '',
  `terminator_ip` varchar(20) DEFAULT '',
  `real_duration` double NOT NULL DEFAULT '0' COMMENT 'exact duration',
  `real_billsec` double NOT NULL DEFAULT '0' COMMENT 'exact billsec',
  `did_billsec` int(11) DEFAULT '0' COMMENT 'billsec for incoming call',
  `dst_user_id` int(11) DEFAULT NULL COMMENT 'users id which receives call',
  PRIMARY KEY (`id`),
  KEY `calldate` (`calldate`,`src_device_id`,`dst_device_id`),
  KEY `src_device_id` (`src_device_id`),
  KEY `dst_device_id` (`dst_device_id`),
  KEY `src` (`src`,`disposition`),
  KEY `provider_id` (`provider_id`) USING BTREE,
  KEY `card_id` (`card_id`) USING BTREE,
  KEY `disposition` (`disposition`) USING BTREE,
  KEY `user_id_index` (`user_id`),
  KEY `hgcause` (`hangupcause`),
  KEY `resellerid` (`reseller_id`),
  KEY `dst_user_id_index` (`dst_user_id`),
  KEY `did_id` (`did_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85937141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-16 14:39:57
