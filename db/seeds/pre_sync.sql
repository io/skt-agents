ALTER TABLE calls   
  CHANGE `account_id` `user_id` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL,
  CHANGE `created_at` `created_at` DATETIME NULL,
  CHANGE `updated_at` `updated_at` DATETIME NULL;

RENAME TABLE users TO users_;

ALTER TABLE accounts   
  CHANGE `created_at` `created_at` DATETIME NULL,
  CHANGE `updated_at` `updated_at` DATETIME NULL;

RENAME TABLE accounts TO users;

