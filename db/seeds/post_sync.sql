ALTER TABLE calls   
  CHANGE `user_id` `account_id` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NULL,
  CHANGE `created_at` `created_at` DATETIME NOT NULL,
  CHANGE `updated_at` `updated_at` DATETIME NOT NULL;
  
UPDATE calls SET created_at = (SELECT NOW()) where created_at = "0000-00-00 00:00:00";  
UPDATE calls SET updated_at = (SELECT NOW());
  
RENAME TABLE users TO accounts;  

RENAME TABLE users_ TO users;

ALTER TABLE accounts   
  CHANGE `created_at` `created_at` DATETIME NOT NULL,
  CHANGE `updated_at` `updated_at` DATETIME NOT NULL;
  
UPDATE accounts SET created_at = (SELECT NOW()) where created_at = "0000-00-00 00:00:00"; 
UPDATE accounts SET updated_at = (SELECT NOW());


