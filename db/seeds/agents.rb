1.upto(14) do |n|
  address = Address.create :address_line_1 => Faker::Address.street_address, :address_country => 'Italy', :address_vat => rand(0..1000)+1000, :address_phone_1 => Faker::PhoneNumber.phone_number
  puts 'created address: ' << n.to_s
  address.save
end

1.upto(14) do |n|
	
  user = User.find_or_create_by_email :name => Faker::Name.name, :accounting_code => n+1, :margin => rand(5..95), :email => Faker::Internet.email, :password => ENV['AGENT_PASSWORD'], :password_confirmation => ENV['AGENT_PASSWORD'], :address_id => n
  puts 'user: ' << user.name
  user.add_role :agent
  user.skip_confirmation!
  user.save(:validate => false)
  
end
