class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.datetime :calldate
      t.string :clid
      t.string :src
      t.string :dst
      t.string :dcontext
      t.string :channel
      t.string :dstchannel
      t.string :lastapp
      t.string :lastdata
      t.integer :duration
      t.integer :billsec
      t.string :disposition
      t.integer :amaflags
      t.string :accountcode
      t.string :uniqueid
      t.string :userfield
      t.integer :src_device_id
      t.integer :dst_device_id
      t.integer :processed
      t.float :did_price
      t.integer :card_id
      t.integer :provider_id
      t.float :provider_rate
      t.integer :provider_billsec
      t.float :provider_price
      t.string :account_id
      t.float :user_rate
      t.integer :user_billsec
      t.float :user_price
      t.integer :reseller_id
      t.float :reseller_rate
      t.integer :reseller_billsec
      t.float :reseller_price
      t.integer :partner_id
      t.float :partner_rate
      t.integer :partner_billsec
      t.float :partner_price
      t.string :prefix
      t.integer :server_id
      t.integer :hangupcause
      t.string :callertype
      t.string :peerip
      t.string :recvip
      t.string :sipfrom
      t.string :uri
      t.string :useragent
      t.string :peername
      t.integer :t38passthrough
      t.float :did_inc_price
      t.float :did_prov_price
      t.string :localized_dst
      t.integer :did_provider_id
      t.integer :did_id
      t.string :originator_ip
      t.string :terminator_ip
      t.float :real_duration
      t.float :real_billsec
      t.integer :did_billsec
      t.integer :dst_user_id

      t.timestamps
    end
  end
end
