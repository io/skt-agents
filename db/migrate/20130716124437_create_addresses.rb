class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :address_country
      t.string :address_line_1
      t.string :address_line_2
      t.string :address_vat
      t.string :address_phone_1
      t.string :address_phone_2
      t.string :address_fax

      t.timestamps
    end
  end
end
