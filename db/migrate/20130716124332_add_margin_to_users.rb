class AddMarginToUsers < ActiveRecord::Migration
  def change
    add_column :users, :margin, :float
  end
end
