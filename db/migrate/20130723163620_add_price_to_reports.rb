class AddPriceToReports < ActiveRecord::Migration
  def change
    add_column :reports, :price, :float
  end
end
