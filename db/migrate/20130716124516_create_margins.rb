class CreateMargins < ActiveRecord::Migration
  def change
    create_table :margins do |t|
      t.datetime :sampled_at
      t.integer :time_window
      t.float :price
      t.float :margin
      t.integer :accounting_code

      t.timestamps
    end
  end
end
