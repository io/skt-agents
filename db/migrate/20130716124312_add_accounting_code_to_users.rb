class AddAccountingCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :accounting_code, :integer
  end
end
