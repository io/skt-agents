class AddCallCountToReports < ActiveRecord::Migration
  def change
    add_column :reports, :call_count, :integer
  end
end
