class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.float :margin
      t.integer :accounting_code
      t.integer :month
      t.integer :year

      t.timestamps
    end
  end
end
