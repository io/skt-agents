class AddDurationSecToReports < ActiveRecord::Migration
  def change
    add_column :reports, :duration_sec, :integer
  end
end
