class AddDurationSecToMargins < ActiveRecord::Migration
  def change
    add_column :margins, :duration_sec, :integer
  end
end
