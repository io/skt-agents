class AddUserNameToMargins < ActiveRecord::Migration
  def change
    add_column :margins, :user_name, :string
  end
end
