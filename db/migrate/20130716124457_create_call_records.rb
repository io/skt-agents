class CreateCallRecords < ActiveRecord::Migration
  def change
    create_table :call_records do |t|
      t.integer :accounting_code
      t.string :user_name
      t.datetime :call_date
      t.integer :bill_sec
      t.float :user_price
      t.string :prefix

      t.timestamps
    end
  end
end
