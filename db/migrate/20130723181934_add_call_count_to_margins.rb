class AddCallCountToMargins < ActiveRecord::Migration
  def change
    add_column :margins, :call_count, :integer
  end
end
