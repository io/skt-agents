class AddUserNameToReports < ActiveRecord::Migration
  def change
    add_column :reports, :user_name, :string
  end
end
