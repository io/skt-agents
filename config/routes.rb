TelefreeAgents::Application.routes.draw do
	

  resources :dashboards


  resources :call_records


  resources :addresses


  resources :calls


  resources :reports


  #resources :addresses


  resources :accounts


  #resources :calls


  resources :margins


  #resources :call_records


  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  authenticated :user do
    root :to => 'reports#index'
  end
  devise_for :users, :controllers => { :registrations => "registrations", :confirmations => "confirmations" }
  as :user do 
    root :to => "devise/sessions#new"    
    get "/login" => "devise/sessions#new"                                 
    post "/login" => "devise/sessions#create"
    delete "/logout" => "devise/sessions#destroy"
    get "/subscribe" => "devise/registrations#new"
    get "/reset" => "devise/passwords#new"
    put "/reset" => "devise/passwords#update"    
    match '/user/confirmation' => 'confirmations#update', :via => :put, :as => :update_user_confirmation
  end 

  resources :users
  match 'resellers' => 'users#index'
  match '*a', :to => 'errors#not_found'
  match '/404', :to => 'errors#not_found'
  match '/422', :to => 'errors#server_error'
  match '/500', :to => 'errors#server_error'
end