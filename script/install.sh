#!/bin/sh     
               
ruby script/reverse_scaffold calls Call              
ruby script/reverse_scaffold accounts Account 
                                            
rails g migration AddAccountingCodeToUsers accounting_code:integer
rails g migration AddMarginToUsers margin:float
rails g migration AddAddressIdToUsers address_id:integer
rails g scaffold Address address_country:string address_line_1:string address_line_2:string address_vat:string address_phone_1:string address_phone_2:string address_fax:string 
rails g scaffold CallRecord accounting_code:integer user_name:string call_date:datetime bill_sec:integer user_price:float prefix:string
rails g scaffold Margin sampled_at:datetime time_window:integer price:float margin:float accounting_code:integer
rails g scaffold Report margin:float accounting_code:integer month:integer year:integer

rails g migration AddUserNameToMargins user_name:string
rails g migration AddPriceToReports price:float
rails g migration AddCallCountToMargins call_count:integer
rails g migration AddCallCountToReports call_count:integer
rails g migration AddDurationSecToMargins duration_sec:integer
rails g migration AddDurationSecToReports duration_sec:integer
       



