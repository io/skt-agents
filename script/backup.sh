#!/bin/sh

rm -Rfv /git/rails_apps/$APP_NAME/
mkdir -v /git/rails_apps/$APP_NAME

cp Gemfile /git/rails_apps/$APP_NAME/
cp -Rfv app/ /git/rails_apps/$APP_NAME/
cp -Rfv config/ /git/rails_apps/$APP_NAME/
cp -Rfv --parents db/schema/ /git/rails_apps/$APP_NAME/
cp -Rfv --parents db/seeds/ /git/rails_apps/$APP_NAME/

rm -v /git/rails_apps/$APP_NAME/app/controllers/application_*.rb
rm -v /git/rails_apps/$APP_NAME/app/controllers/errors_*.rb
rm -v /git/rails_apps/$APP_NAME/config/cucumber.yml
rm -v /git/rails_apps/$APP_NAME/config/boot.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/backtrace_silencers.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/generators.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/rolify.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/secret_token.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/session_store.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/simple_form.rb
rm -v /git/rails_apps/$APP_NAME/config/initializers/wicked_pdf.rb   
rm -v /git/rails_apps/$APP_NAME/config/initializers/wrap_parameters.rb
rm -Rfv /git/rails_apps/$APP_NAME/config/locales

zip -9 -r /git/rails_apps/$APP_NAME.zip /git/rails_apps/$APP_NAME/
                                              
