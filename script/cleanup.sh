#!/bin/sh     
         
rm -fv /srv/$APP_NAME/app/controllers/address* 
                                                    
rm -fv /srv/$APP_NAME/app/assets/stylesheets/*.scss
rm -fv /srv/$APP_NAME/app/assets/javascripts/*.coffee
rm -fv /srv/$APP_NAME/app/helpers/address* 
rm -fv /srv/$APP_NAME/app/helpers/calls*
rm -fv /srv/$APP_NAME/app/helpers/accounts*
rm -fv /srv/$APP_NAME/app/helpers/margin*
rm -fv /srv/$APP_NAME/app/helpers/call_record* 
rm -fv /srv/$APP_NAME/app/helpers/report* 

rm -fv /srv/$APP_NAME/app/views/addresses/_form* 
rm -fv /srv/$APP_NAME/app/views/addresses/edit* 
rm -fv /srv/$APP_NAME/app/views/addresses/new*
rm -fv /srv/$APP_NAME/app/views/addresses/show*  
rm -fv /srv/$APP_NAME/app/views/accounts/_form* 
rm -fv /srv/$APP_NAME/app/views/accounts/edit* 
rm -fv /srv/$APP_NAME/app/views/accounts/new*
rm -fv /srv/$APP_NAME/app/views/accounts/show*  
rm -fv /srv/$APP_NAME/app/views/calls/_form* 
rm -fv /srv/$APP_NAME/app/views/calls/edit* 
rm -fv /srv/$APP_NAME/app/views/calls/new*
rm -fv /srv/$APP_NAME/app/views/calls/show*     
rm -fv /srv/$APP_NAME/app/views/call_records/_form* 
rm -fv /srv/$APP_NAME/app/views/call_records/edit* 
rm -fv /srv/$APP_NAME/app/views/call_records/new*
rm -fv /srv/$APP_NAME/app/views/call_records/show*  
rm -fv /srv/$APP_NAME/app/views/margins/_form* 
rm -fv /srv/$APP_NAME/app/views/margins/edit* 
rm -fv /srv/$APP_NAME/app/views/margins/new*
rm -fv /srv/$APP_NAME/app/views/margins/show*  
rm -fv /srv/$APP_NAME/app/views/reports/_form* 
rm -fv /srv/$APP_NAME/app/views/reports/edit* 
rm -fv /srv/$APP_NAME/app/views/reports/new*
rm -fv /srv/$APP_NAME/app/views/reports/show*  

       



