$(document).ready(function () { fade_flash(); });

var fade_flash = function() {
    if ($(".alert-box").css('display').localeCompare("block") == 0) { $('.alert-box [class^="alert"]').delay(2000).slideUp(); }
};

var show_flash = function(msg, type) {
    $(".alert-box").html('<div class="alert-'+type+'">'+msg+'</div>');
    fade_flash();
};

$(document).ajaxComplete(function(event, request) {
    var msg = request.getResponseHeader('X-Message');
    var type = request.getResponseHeader('X-Message-Type'); 
    if (msg) { show_flash(msg, type); }
});
