var oTable = $('#tbl_margins').dataTable({                                                         
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,          
  "bProcessing": true, 
  "aoColumns": [
      null,
      null,
      { "sClass": "center" },
      { "sClass": "center" },
      { "sClass": "center" }
  ],  
  "sAjaxSource": $('#tbl_margins').data('source'),
  "fnServerData": function ( sSource, aoData, fnCallback ) {
  	aoData.push({ "name": "acc_code", "value": $('#acc_code').html() });
  	aoData.push({ "name": "can_view", "value": $('#can_view').html() });
  	$.getJSON( sSource, aoData, function (json) { 
  		fnCallback(json)
  	});
  },
  "fnDrawCallback" : function() {
    $('.editable').editable();
    $('#row_counter').text($('#tbl_margins').dataTable().fnSettings().fnRecordsDisplay())
  }  
})



$('#tbl_margins_filter input').datepicker({
     minViewMode: 0,
     format: 'dd MM, yyyy', 
     autoclose: true        
}).keyup( function () {
  oTable.fnFilter( this.value );
}).on('changeDate', function(e){
        var y = e.date.getFullYear(),
            _m = e.date.getMonth() + 1,
            m = (_m > 9 ? _m : '0'+_m),
            _d = e.date.getDate(),
            d = (_d > 9 ? _d : '0'+_d);
        oTable.fnFilter(y + '-' + m + '-' + d);
});  




$('#tbl_margins_filter input').width('200px')

  
  
$('#tbl_margins_filter')
 .append('<span class="add-on" style="font-size:18px;"><i class="icon-calendar"></i></span>')
 .addClass('input-append date')
 .width('200px')  
 .children('label')
  .width('199px')
  
$('.add-on .icon-calendar')
 .height('20px')
 .width('20px')       
 
$('.btn-alfa-3 span')
 .hide()
 .css('margin-left','5px')
 .css('font-size','13px')
 
$('.btn-alfa-3').hover(
  function () {
   $(this).find('span').toggle();
  }
); 
