var oTable = $('#tbl_reports').dataTable({                                                                                               
  "sPaginationType": "bootstrap",
  "bLengthChange": false,                                                                       
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',  
  "bServerSide": true,
  "bProcessing": true,
  "fnServerData": function ( sSource, aoData, fnCallback ) {
  	aoData.push({ "name": "acc_code", "value": $('#acc_code').html() });
  	aoData.push({ "name": "can_view", "value": $('#can_view').html() });
  	$.getJSON( sSource, aoData, function (json) { 
  		fnCallback(json)
  	});
  },  
  "sAjaxSource": $('#tbl_reports').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable();
    $('#row_counter').text($('#tbl_reports').dataTable().fnSettings().fnRecordsDisplay())
  }  
})

$('#tbl_reports_filter input').datepicker({
     minViewMode: 1, 
     format: 'MM yyyy', 
     autoclose: true        
}).keyup( function () {
  oTable.fnFilter( this.value );
}).on('changeDate', function(e){
        var y = e.date.getFullYear(),
            _m = e.date.getMonth() + 1,
            m = (_m > 9 ? _m : '0'+_m);
            oTable.fnFilter(y + '-' + m + '-01');
       
});  



$('#tbl_reports_filter input')
 .width('172px')
 


$('#tbl_reports_filter')
 .append('<span class="add-on" style="font-size:18px;"><i class="icon-calendar"></i></span>')
 .addClass('input-append date')
 .width('172px')  
 .children('label')
  .width('171px')

$('.add-on .icon-calendar')
 .height('20px')
 .width('20px')  
 
$('.btn-alfa-3 span')
 .hide()
 .css('margin-left','5px')
 .css('font-size','13px')
 
$('.btn-alfa-3').hover(
  function () {
   $(this).find('span').toggle();
  }
);
