$('#tbl_call_records').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",                                                                                                    
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "iDisplayLength": 10,
  "sAjaxSource": $('#tbl_call_records').data('source'),
  "fnDrawCallback" : function() {
    $('.editable').editable();
    $('#row_counter').text($('#tbl_call_records').dataTable().fnSettings().fnRecordsTotal())
  }  
})



