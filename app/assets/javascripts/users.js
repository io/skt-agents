var oTable;
 
/* Formating function for row details */
function fnFormatDetails ( nTr )
{
    var aData = oTable.fnGetData( nTr );
    
    var sOut = '<div class="well well-small pull-left span3" style="margin-top:0px;margin-bottom:0px;">'
    sOut += '<h4>Address</h4>';
    sOut += '<div class="rule"></div><p>';
    sOut += aData[7] + '<br>'; 
    sOut += aData[8] + '<br>'; 
    sOut += 'VAT Reg. No. ' + aData[9]; 
    sOut += '</p></div>'
    return sOut;
}

oTable = $('#tbl_users').dataTable({
                                                                                                
  "sPaginationType": "bootstrap",
  "bLengthChange": false,
  "sDom": '<"tbl_info pull-left"fr>t<"tbl_footer"p>',
  "bServerSide": true,
  "bProcessing": true,
  "iDisplayLength": 10,
  "aoColumns": [
      { "sClass": "center", "bSortable": false },
      null,
      null,
      { "sClass": "center" },
      { "sClass": "center" },
      { "sClass": "center", "bSortable": false }
  ],
  "aaSorting": [[1, 'asc']],  
  "sAjaxSource": $('#tbl_users').data('source'),
  "fnDrawCallback" : function() {

    $('.editable').editable();
    $('.hidden-details').on( 'click', function () {  
    		
        var nTr = $(this).parents('tr')[0];
        
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            $(this).children('i')
              .removeClass('icon-minus-sign-alt')
              .addClass('icon-plus-sign-alt') 
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            $(this).children('i')
              .removeClass('icon-plus-sign-alt')
              .addClass('icon-minus-sign-alt') 
            oTable.fnOpen( nTr, fnFormatDetails(nTr), 'details' );
        }
    } );        
    $('#row_counter').text($('#tbl_users').dataTable().fnSettings().fnRecordsDisplay())
  }  
})


$('.modal .modal-footer :submit').button()
$('.modal .modal-footer :submit').on("click", function () {
		$(this).button('loading')
});


