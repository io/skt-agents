class Address < ActiveRecord::Base
	# ----------------------------------------------------------------------
  	attr_accessible :address_country, :address_line_1, :address_line_2, :address_vat, :address_phone_1, :address_phone_2, :address_fax
  	# ----------------------------------------------------------------------
  	has_one :user
  	# ----------------------------------------------------------------------
  	
  	validates_presence_of :address_country, :message => 'Please select a Country!' 
  	validates_presence_of :address_line_1, :message => 'Please specify City, Street Address and ZIP Code!'
  	validates_presence_of :address_vat, :message => 'Please specify a VAT/TAX number!'
  	validates_presence_of :address_phone_1, :message => 'Please specify a Phone Number!'
  	validates_uniqueness_of :address_vat, :message => 'A User with the VAT specified already exists!'
  
	# ----------------------------------------------------------------------
  	default_values  :address_line_1 => { :value => lambda { Faker::Address.street_address }, :allows_nil => false }
  			#:address_vat => { :value => lambda { rand(1000..10000)*427 }, :allows_nil => false },
  			#:address_phone_1 => { :value => lambda { Faker::PhoneNumber.phone_number }, :allows_nil => false }
  			
end
