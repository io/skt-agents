class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new 
    alias_action :create, :read, :update, :destroy, :to => :crud
    
    if user.has_role? :admin
    	    can :manage, :all
    elsif user.has_role? :owner
    	    can :manage, [Report, User, Account, Address, Dashboard]
    	    cannot :manage, [Margin, Call, CallRecord]
    else
    	    can :manage, [Dashboard, Report] 
    	    cannot :manage, [Margin, User, Account, Call, CallRecord, Address]
    end
  end
  
end
