class Report < ActiveRecord::Base
  
  
	belongs_to :user, :primary_key => :accounting_code, :foreign_key => :accounting_code
	
	attr_accessible :accounting_code, :margin, :month, :year, :price, :user_name, :call_count, :duration_sec
        
	validates :accounting_code, :margin, :month, :year, :presence, :price, :user_name, :call_count, :duration_sec, :presence => true 
	
        # ----------------------------------------------------------------------
	def self.sync
		begin
		  		  	  
		  @stats = Margin.all.group_by{ |s| [s.accounting_code, s.user_name] }.map {|k,v| [k.first, k.last,v]}
		  @stats.each do |code, user, margins|
		      @report = Report.find_or_create_by_year_and_month_and_accounting_code_and_user_name(:year => Time.new.strftime('%Y'), :month => Time.new.strftime('%m'), :accounting_code => code, :user_name => user)
		      x = 0
		      y = 0
		      z = 0
		      w = 0
		      margins.each do |m|
		      	      x += m.margin
		      	      y += m.price 
		      	      z += m.call_count
		      	      w += m.duration_sec
		      end        
		      @report.margin = x
		      @report.price = y
		      @report.call_count = z
		      @report.duration_sec = w
		      @report.save		      
	          end
	          return true 		  
	        rescue                                                          
	          return false                                                  
	        end 
	end 	
	
	
end                                                                                                                                                                         
