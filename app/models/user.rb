class User < ActiveRecord::Base            
	# ----------------------------------------------------------------------
	resourcify
	rolify
	belongs_to :role
	belongs_to :address
	has_many :reports, :primary_key => :accounting_code, :foreign_key => :accounting_code
	accepts_nested_attributes_for :address, :allow_destroy => true	
	devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable, :validatable
	# ---------------------------------------------------------------------- 
        validates :name, :email, :accounting_code, :margin, :presence => true	
	validates :accounting_code, :numericality => { :greater_than_or_equal_to => 1, :less_than_or_equal_to => 999, :message => 'Accounting Code must be 3 digits!'}
	validates :margin, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100, :message => 'Margin is a % between 1 and 100!'} 
	validates :email, :format => { :with => /^([A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})?$/, :message => 'Email must be valid!' }
	validates_uniqueness_of :accounting_code, :message => 'Accounting Code must be unique!'
	validates_uniqueness_of :email, :message => 'A User with that email already exists!'
	# ----------------------------------------------------------------------
	before_create :set_default_role	                              
	attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :margin, :accounting_code, :created_at, :updated_at, :address_attributes, :address_id   
	# ----------------------------------------------------------------------
  	default_values  :email => { :value => lambda { Faker::Internet.email}, :allows_nil => false },
  			:accounting_code => { :value => lambda { (sprintf '%03d', rand(30..995))}, :allows_nil => false },
  			:name => { :value => lambda { Faker::Name.name}, :allows_nil => false },
  			:margin => { :value => lambda { rand(1..99) }, :allows_nil => false }  
	# ----------------------------------------------------------------------
  	def set_default_role                              
  	   self.add_role Role.find_by_name('agent')
  	end
        # ----------------------------------------------------------------------
	def role?(role_sym)                                                     
	  roles.any? { |r| r.name.underscore.to_sym == role_sym }
	end      
	# ----------------------------------------------------------------------
	# no password is required when the account is created; validate password when the user sets one
	# ----------------------------------------------------------------------
	validates_confirmation_of :password                                     
	def password_required?                                                  
	  if !persisted?                                                        
	    !(password != "")                                                   
	  else                                                                  
	    !password.nil? || !password_confirmation.nil?                       
	  end                                                                   
	end                                                                     
	# ----------------------------------------------------------------------
	# Send reset password instructions                                      
	# ----------------------------------------------------------------------
	def send_reset_password_instructions                                    
	  if self.confirmed?                                                    
	    super                                                               
	  else                                                                  
	    errors.add :base, "You must receive an invitation before you set your password."
	  end
	end  
	# ----------------------------------------------------------------------
	# override Devise method: no password confirmation required             
	# ----------------------------------------------------------------------
	def confirmation_required?                                              
	  false                                                                 
	end                                                                     
	# ----------------------------------------------------------------------
	# override Devise method: can auth only if confirmed or confirmation period has not expired
	# ----------------------------------------------------------------------
	def active_for_authentication?                                          
	  confirmed? || confirmation_period_valid?                              
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to set the password                                      
	# ----------------------------------------------------------------------
	def attempt_set_password(params)                                        
	  p = {}                                                                
	  p[:password] = params[:password]                                      
	  p[:password_confirmation] = params[:password_confirmation]            
	  update_attributes(p)                                                  
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to determine whether a password has been set             
	# ----------------------------------------------------------------------
	def has_no_password?                                                    
	  self.encrypted_password.blank?                                        
	end                                                                     
	# ----------------------------------------------------------------------
	# new function to provide access to protected method pending_any_confirmation
	# ----------------------------------------------------------------------
	def only_if_unconfirmed                                                 
	  pending_any_confirmation {yield}                                      
	end                                                                     
	# ----------------------------------------------------------------------
	# ----------------------------------------------------------------------
	private                                                                 
	# ----------------------------------------------------------------------
	def send_welcome_email                                                  
	  unless self.email.include?(ENV['ADMIN_EMAIL'])    
	    UserMailer.welcome_email(self.email).deliver                        
	  end 	                                                                
	end                                                                     
	# ----------------------------------------------------------------------
end
