class Margin < ActiveRecord::Base
	
	
	attr_accessible :accounting_code, :margin, :sampled_at, :time_window, :price, :user_name, :call_count, :duration_sec
        
	validates :accounting_code, :margin, :sampled_at, :time_window, :price, :duration, :presence => true
  
  	default_values :time_window => { :value => 60, :allows_nil => false }
  			
  	
	def self.sync
		begin  			
		  #CallRecord.where{call_date.gte (Call.first.created_at - 1.hour)}.group_by(&:accounting_code).each do |code, call_records|
		  @stats = CallRecord.where{call_date.gte (Call.first.created_at - 1.hour)}.group_by{ |s| [s.accounting_code, s.user_name] }.map {|k,v| [k.first, k.last, v.length]}
		  
		  @stats.each do |code, user, calls|
		    price = CallRecord.expenditure(CallRecord.where{ user_name.eq user })	
		    duration = CallRecord.duration(CallRecord.where{ user_name.eq user })
		    margin_percentage = User.where{ accounting_code.eq code }.first.margin		    
		    margin = price.to_f * margin_percentage.to_f / 100.0
		    Margin.create :sampled_at => Time.zone.now, :accounting_code => code, :margin => margin, :price => price, :user_name => user, :call_count => calls, :duration_sec => duration
		  end                                                                                            
	          return true 		  
	        rescue                                                          
	          return false                                                  
	        end 
	end 			
  			
  			
end
