class CallRecord < ActiveRecord::Base
  	attr_accessible :accounting_code, :bill_sec, :call_date, :prefix, :user_name, :user_price

  	# ----------------------------------------------------------------------
  	validates :accounting_code, :bill_sec, :call_date, :prefix, :user_name, :user_price, :presence => true
  	# ----------------------------------------------------------------------

  	# ----------------------------------------------------------------------
        def self.duration_by_accounting_code(code)         
        	i = 0
                CallRecord.where{accounting_code.eq code}.each do |m|
		    i += m.bill_sec		    
	        end
	        return i                        
        end
  	# ----------------------------------------------------------------------
        def self.expenditure_by_accounting_code(code)         
        	i = 0
                CallRecord.where{accounting_code.eq code}.each do |m|
		    i += m.user_price 	    
	        end
	        return i                        
        end    
        # ----------------------------------------------------------------------
        def self.expenditure(call_records)
        	i = 0
        	call_records.each do |m|
        		i += m.user_price
        	end
        	return i
        end
        # ----------------------------------------------------------------------
        def self.duration(call_records)
        	i = 0
        	call_records.each do |m|
        		i += m.bill_sec
        	end
        	return i
        end        
   	# ----------------------------------------------------------------------
	def self.sync
		begin       
		  Call.where{billsec.gt 0}.each do |c|
		    a = c.account
		    if a.accounting_number.length > 0
		    	    CallRecord.create :accounting_code => a.accounting_number.to_i, :bill_sec => c.user_billsec, :call_date => c.calldate, :prefix => c.prefix, :user_name => a.first_name, :user_price => c.user_price
	            end
		  end                                                                                            
	          return true 		  
	        rescue                                                          
	          return false                                                  
	        end 
	end  
        # ----------------------------------------------------------------------
end
