class DashboardsController < InheritedResources::Base
  	before_filter :authenticate_user! 
  	load_and_authorize_resource        
        respond_to :html, :json, :js, :xls, :only => :index
        
  	# **********************************************************************
  	#                                                                
  	# **********************************************************************
  	def index     
  	  if (current_user.has_role? :admin or current_user.has_role? :owner)  
  	     @resellers = User.all
  	     @reports = Report.all.group_by(&:user_name)
          else   
             @resellers = current_user
             @reports = Report.where(accounting_code: current_user.accounting_code).group_by(&:user_name)
          end   		
          @response = {:resellers => @resellers, :reports => @reports}
	  respond_with @response  
  	  
  	end 		
end
