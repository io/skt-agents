class AddressesController < InheritedResources::Base
  	before_filter :authenticate_user! 
  	load_and_authorize_resource
  	respond_to :html, :json, :js
  	# **********************************************************************
  	def index
  	  respond_with do |format|
  	    format.json { render json: AddressesDatatable.new(view_context) }   
  	  end
  	end                                                             
  	# ----------------------------------------------------------------------
  	def update    
  	  @address = Address.find(params[:id])  
  	  @address.update_attributes(params[:address])     
  	  flash[:notice] = "Address was successfully updated." if @address.save
  	  respond_with @address
  	end  	
end
