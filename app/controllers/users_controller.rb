class UsersController < ApplicationController
  	# **********************************************************************
  	before_filter :authenticate_user! 
  	load_and_authorize_resource        
        respond_to :html, :json, :js
  	# **********************************************************************
  	def index
  	  @user = User.new
  	  @user.build_address
  	  respond_with do |format|
  	    format.json { render json: UsersDatatable.new(view_context) }   
  	  end
  	end                                                             
  	# ----------------------------------------------------------------------
  	def update    
  	  @user = User.find(params[:id])  
  	  @user.update_attributes(params[:user])     
  	  flash[:notice] = "Reseller was successfully updated." if @user.save
  	  respond_with @user
  	end   
  	# ----------------------------------------------------------------------
  	def destroy   
  	  @user = User.find(params[:id])
  	  respond_to do |format|
  	    if @user.destroy
  	      flash[:notice] = "Reseller was successfully deleted."
  	      format.js {}
  	      format.json { render json: UsersDatatable.new(view_context) }
  	    else
  	      format.html { render action: "index" }
  	      format.json { render json: @user.errors, status: :unprocessable_entity }
  	    end
  	  end    
  	end                                                                     
  	# ----------------------------------------------------------------------
end
