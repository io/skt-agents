class RegistrationsController < Devise::RegistrationsController                      
	respond_to :html, :json, :js
	# **********************************************************************      
	# Actions                                                               
	# **********************************************************************
        def new
          resource = build_resource({})
          resource.build_address
          respond_with resource
        end
	# ----------------------------------------------------------------------
	# ovverride #create to respond to AJAX with a partial                   
	# ----------------------------------------------------------------------
	def create  
          resource = build_resource(params[:user])
	  resource.add_role :agent
	  
  	  respond_to do |format|
  	    if resource.save
  	    	#@user.send_confirmation_instructions     
  	    	resource.send_confirmation_instructions  
  	        flash[:notice] = "Agent was successfully created."
  	        format.js {}    	            
  	        format.json { render json: resource, status: :created, location: resource }

  	    else
  	      clean_up_passwords resource
  	      messages = resource.errors.full_messages.map { |msg| flash.now[:error] = msg }.join 
  	      format.json { render json: messages.titleize, status: :unprocessable_entity }
  	    end
  	  end  		
	end  
	
	# **********************************************************************
	# Private                                                               
	# **********************************************************************
	protected                                                               
	  # --------------------------------------------------------------------
	  # This is rendered if AJAX is not used                                
	  # --------------------------------------------------------------------
	  def after_inactive_sign_up_path_for(resource)                         
	    redirect_to main_app.root_path                                                         
	  end                                                                   
	  # --------------------------------------------------------------------
	  def after_sign_up_path_for(resource)                                  
	    # the page new users will see after sign up (after launch, when no invitation is needed)
	    redirect_to main_app.root_path
	  end
	  # --------------------------------------------------------------------
end
