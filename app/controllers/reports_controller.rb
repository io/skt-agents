require 'csv'

class ReportsController < InheritedResources::Base
  	before_filter :authenticate_user! 
  	load_and_authorize_resource        
        respond_to :html, :json, :js, :xls, :only => :index
        
  	# **********************************************************************
  	#                                                                
  	# **********************************************************************
  	def index             
  	  respond_with do |format|
  	    format.json { render json: ReportsDatatable.new(view_context) }   
  	    if (current_user.has_role? :admin or current_user.has_role? :owner)  	
  	      @reports = Report.all
  	      format.xls {
  	      	      #hash = ActiveRecord::Base.connection.select_all("SELECT u.name as Reseller, r.year, r.month, r.user_name as Customer, r.price FROM reports as r JOIN users as u ON r.accounting_code = u.accounting_code")
  	      	      
  	      	      ##send_data(rec.to_xls(:only => [:user :name, :year, :month, :user_name, :price]))
		      #csv_string = CSV.generate(col_sep: "\t") do |csv|
		      #  csv << hash.first.keys
		      #  hash.each do |x|
		      #    csv << x.values
		      #  end
		      #end
		      #send_data(csv_string)
  	      }
  	      
            else 
              format.xls { send_data(Report.where("accounting_code = ?", current_user.accounting_code).to_xls(:only => [:year, :month, :user_name, :price])) }
            end
  	  end
  	end 	
		
end

