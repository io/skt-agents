class MarginsController < InheritedResources::Base
  	before_filter :authenticate_user! 
  	load_and_authorize_resource        
        respond_to :html, :json, :js, :xls, :only => :index
        
  	# **********************************************************************
  	#                                                                
  	# **********************************************************************
  	def index
  	  respond_with do |format|
  	    format.json { render json: MarginsDatatable.new(view_context) }   
  	    if (current_user.has_role? :admin or current_user.has_role? :owner)
  	      format.xls { send_data(Margin.all.to_xls) }
  	    else
  	      format.xls { send_data(Margin.where("accounting_code = ?", current_user.accounting_code).to_xls) }    
  	    end
  	  end
  	end 	
  	
end
