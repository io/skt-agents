class UserMailer < ActionMailer::Base
	
  default :from => ENV["EMAIL_ADDRESS"]  
  default :cc => ENV["EMAIL_ADDRESS"]
  def welcome_email(to)
    mail(:to => to, :subject => "#{ t 'pages.users.userlist.welcome_email_subject' }")
    headers['X-MC-GoogleAnalytics'] = ENV["DOMAIN"]
    headers['X-MC-Tags'] = "#{ t 'pages.users.userlist.welcome_email_subject' }"    
  end
  
end
