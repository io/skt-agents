class AddressesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Address.count,                                            
      iTotalDisplayRecords: addresses.total_entries,
      aaData: data
    }
  end

private

  def data
    addresses.map do |address|
        [
            h(User.where(address_id: address.id).first.name),
            link_to(address.address_country, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_country", :url => "/addresses/#{address.id}"}),            	    
            link_to(address.address_line_1, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_line_1", :url => "/addresses/#{address.id}"}),
            #link_to(address.address_line_2, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_line_2", :url => "/addresses/#{address.id}"}),
            link_to(address.address_vat, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_vat", :url => "/addresses/#{address.id}"}),
            link_to(address.address_phone_1, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_phone_1", :url => "/addresses/#{address.id}"}),
            #link_to(address.address_phone_2, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_phone_2", :url => "/addresses/#{address.id}"}),
            link_to(address.address_fax, "#", :class => "editable", :data => {:type => "text", :model => "address", :name => "address_fax", :url => "/addresses/#{address.id}"})
        ]
    end
  end

  def addresses
    @addresses ||= fetch_addresses
  end

  def fetch_addresses
    addresses = Address.order("#{sort_column} #{sort_direction}")
    addresses = addresses.page(page).per_page(per_page)
    if params[:sSearch].present?
      addresses = addresses.where("address_country like :search address_line_1 like :search or address_vat like :search or address_phone_1 like :search", search: "%#{params[:sSearch]}%")
    end
    addresses
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[address_country address_line_1 address_line_2 address_vat address_phone_1 address_phone_2 address_fax]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
