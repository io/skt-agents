class UsersDatatable
  delegate :params, :h, :link_to, :distance_of_time_in_words, :number_to_percentage, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: User.with_role(:agent).count,                                            
      iTotalDisplayRecords: users.total_entries,
      aaData: data
    }
  end

private

  def data
    users.map do |user|
        [
            link_to("#", :class => "pull-right hidden-details" ) do
              ("<i class='font-awesome icon-plus-sign-alt'></i>").html_safe
            end,              
            link_to(user.name.titleize, "#", :class => "editable", :data => {:type => "text", :model => "user", :name => "name", :url => "/users/#{user.id}"}),
            link_to(user.email, "#", :class => "editable", :data => {:type => "text", :model => "user", :name => "email", :url => "/users/#{user.id}"}),                                                                                                           
            user.accounting_code > 0 ? link_to((sprintf '%03d', user.accounting_code), "#", :class => "editable", :data => {:type => "text", :model => "user", :name => "accounting_code", :url => "/users/#{user.id}"}) : h("N/A"),
            link_to(number_to_percentage(user.margin, :separator => ",", :precision => 2), "#", :class => "editable", :data => {:type => "text", :model => "user", :name => "margin", :url => "/users/#{user.id}"}),
      
            if user.accounting_code > 0 
            	link_to("#", :class => "pull-right btn btn-hem-5", :data => {:toggle => "modal", :target => "#user_#{user.id}"} ) do
            	  ("<i class='font-awesome icon-trash'></i>").html_safe
              	end
            end,
            user.confirmed_at ? distance_of_time_in_words(Time.zone.now, user.confirmed_at).titleize + " ago" : h("Not yet"),
             
            user.address.address_line_1 ? h(user.address.address_line_1 + ', ' + user.address.address_country) : h("N/A"), 
            user.address.address_phone_1 ? h(user.address.address_phone_1) : h("N/A"),
            user.address.address_vat ? h(user.address.address_vat) : h("N/A") 
            
         	  
        ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = User.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:sSearch].present?
      users = users.where("users.name like :search or email like :search", search: "%#{params[:sSearch]}%")
    end
    users
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[no name email accounting_code margin no]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
