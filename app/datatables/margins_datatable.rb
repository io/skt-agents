
class MarginsDatatable
  delegate :params, :h, :number_to_currency, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Margin.count,
      iTotalDisplayRecords: margins.total_entries,
      aaData: data
    }
  end

private

  def data
    
    margins.map do |margin|
    	    
      		[
      		  h(User.find_by_accounting_code(margin.accounting_code).name),
                  h(margin.sampled_at.strftime('%d %B %Y, %H:%M')),
                  number_to_currency(margin.price, :separator => ",", :precision => 2, :locale => 'it'),
                  number_to_currency(margin.margin, :separator => ",", :precision => 2, :locale => 'it'),
                  h(margin.call_count.to_s)
                  
      		]
      	   end
   
  end

  def margins
    @margins ||= fetch_margins
  end

  def fetch_margins
  	  
    if params[:can_view] == "all"
       margins = Margin.limit(params[:iDisplayLength]).offset(params[:iDisplayStart]).order("#{sort_column} #{sort_direction}")    
    else
       margins = Margin.limit(params[:iDisplayLength]).offset(params[:iDisplayStart]).order("#{sort_column} #{sort_direction}").where("accounting_code = ?", params[:acc_code])    
       
    end

  	  	  
    
    margins = margins.page(page).per_page(per_page)
   
    if params[:sSearch].present? 
      margins = margins.where("sampled_at between :search and :search_o", search: "#{params[:sSearch]} 00:00", search_o: "#{params[:sSearch]} 23:59")
    end
    margins
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[accounting_code sampled_at price margin call_count]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
