class ReportsDatatable
  delegate :params, :h, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Report.count,
      iTotalDisplayRecords: reports.total_entries,
      aaData: data
    }
  end

private

  def data
    
    if params[:can_view] == "all" 
    	    
      reports.map do |report|
      	        
        		[
        		  h(User.find_by_accounting_code(report.accounting_code).name),
        		  h(report.user_name.titleize),
        		  h(DateTime.new(report.year,report.month).strftime('%B')),
                    h(report.year),
                    number_to_currency(report.price, :separator => ",", :precision => 2, :locale => 'it'),
                    number_to_currency(report.margin, :separator => ",", :precision => 2, :locale => 'it')
                    
        		]
      end 
    else
      reports.map do |report|
      	        
        		[        		 
        		  h(report.user_name.titleize),
        		  h(DateTime.new(report.year,report.month).strftime('%B')),
                    h(report.year),
                    number_to_currency(report.price, :separator => ",", :precision => 2, :locale => 'it')
                    
        		]
      end     	    
    	    
    end
 
  end

  def reports
    @reports ||= fetch_reports
  end

  def fetch_reports
  	  
    if params[:can_view] == "all" 
    	    reports = Report.limit(params[:iDisplayLength]).offset(params[:iDisplayStart]).order("#{sort_column} #{sort_direction}")
    else
    	    reports = Report.limit(params[:iDisplayLength]).offset(params[:iDisplayStart]).order("#{sort_column} #{sort_direction}").where("accounting_code = ?", params[:acc_code])
    end
    
    reports = reports.page(page).per_page(per_page)
   
    if params[:sSearch].present? 
      reports = reports.where("month = :month_s AND year = :year_s", month_s: "#{params[:sSearch][5..6]}", year_s: "#{params[:sSearch][0..3]}")
    else
       	    
    end
   
    reports
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[accounting_code user_name month year price margin]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
