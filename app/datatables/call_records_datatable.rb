class CallRecordsDatatable
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: CallRecord.count,
      iTotalDisplayRecords: call_records.total_entries,
      aaData: data
    }
  end

private

  def data
    
    call_records.map do |call_record|
    	    
      		[
      		  h(call_record.accounting_code),
      		  h(call_record.user_name),
      		  h(call_record.call_date.strftime('%d/%m/%y %H:%M ')),
      		  h(call_record.bill_sec),   
      		  number_to_currency(call_record.user_price, :separator => ",", :precision => 6, :locale => 'it')
      		]
      	   end
   
  end

  def call_records
    @call_records ||= fetch_call_records
  end

  def fetch_call_records
    call_records = CallRecord.limit(params[:iDisplayLength]).offset(params[:iDisplayStart]).order("#{sort_column} #{sort_direction}")
    call_records = call_records.page(page).per_page(per_page)
    if params[:sSearch].present?
      call_records = call_records.where("accounting_code like :search or user_name like :search", search: "%#{params[:sSearch]}%")
    
    	    
    end
    call_records
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[accounting_code user_name call_date bill_sec user_price]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
