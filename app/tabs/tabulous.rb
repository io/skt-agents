Tabulous.setup do

  tabs(:app) do	  
  	  
    dashboards_tab do
      text          { 'Margins' }
      link_path     { dashboards_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('dashboards') }
    end    	  

    reports_tab do
      text          { 'Reports' }
      link_path     { reports_path }
      visible_when  { true }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('reports') }
    end    
    
    margins_tab do
      text          { 'Details' }
      link_path     { margins_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('margins') }
    end  
    
  end
  
  tabs(:admin) do
  
    
    users_tab do
      text          { 'Resellers' }
      link_path     { resellers_path }
      visible_when  { current_user.has_role? :admin or current_user.has_role? :owner }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('users') }
    end    
    
    addresses_tab do
      text          { 'Addresses' }
      link_path     { addresses_path }
      visible_when  { current_user.has_role? :admin or current_user.has_role? :owner }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('addresses') }
    end      
    
  end
  
  tabs(:mor) do
  	  
    accounts_tab do
      text          { 'Customers' }
      link_path     { accounts_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('accounts') }
    end  
    
    call_records_tab do
      text          { 'Call Records' }
      link_path     { call_records_path }
      visible_when  { current_user.has_role? :admin }
      enabled_when  { true }
      active_when   { in_action('any').of_controller('call_records') }
    end   
    
    
  	  
  end
  



  customize do

    # which class to use to generate HTML
    # :default, :html5, :bootstrap, or :bootstrap_pill
    # or create your own renderer class and reference it here
    renderer :custom

    # whether to allow the active tab to be clicked
    # defaults to true
    # active_tab_clickable true

    # what to do when there is no active tab for the currrent controller action
    # :render -- draw the tabset, even though no tab is active
    # :do_not_render -- do not draw the tabset
    # :raise_error -- raise an error
    # defaults to :do_not_render
    when_action_has_no_tab :render

    # whether to always add the HTML markup for subtabs, even if empty
    # defaults to false
    # render_subtabs_when_empty false

  end



end
