class CustomRenderer < Tabulous::DefaultRenderer

  def tabs_html
    <<-HTML.strip_heredoc
        <ul class="nav aside">
          #{ tab_list_html }
        </ul>
    HTML
  end


protected

    def tab_html(tab)
      return '' unless tab.visible?(@view)
      html = ''
      klass = ''
      aid = 'btn_'
      klass << 'active' if tab.active?(@view)
      klass << ' disabled' unless tab.enabled?(@view)
      klass.strip!
      aid << tab.text(@view).downcase
      if klass.empty?
        html << %Q{<li id="#{aid}">}
      else
        html << %Q{<li id="#{aid}" class="#{klass}">}
      end
      html << tab_link(tab)
      html << "</li>"
      html
    end

   def tab_link(tab)
      html = ''
      klass = 'zurbicon-'
      klass << tab.text(@view).downcase
      klass.strip!
      if tab.clickable?(@view) && tab.enabled?(@view)
        html << %Q{<a href="#{tab.link_path(@view)}"><i class="#{klass}"></i><span>#{tab.text(@view)}</span></a>}
      else
        html << %Q{<a><i class="#{klass}"></i><span>#{tab.text(@view)}</span></a>}
      end
      html
    end
    
end
