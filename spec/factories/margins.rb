# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :margin do
    sampled_at "2013-07-16 14:45:16"
    time_window 1
    price 1.5
    margin 1.5
    accounting_code 1
  end
end
