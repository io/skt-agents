# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :report do
    margin 1.5
    accounting_code 1
    month 1
    year 1
  end
end
