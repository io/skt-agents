# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :call do
    calldate "2013-07-16 14:41:55"
    clid "MyString"
    src "MyString"
    dst "MyString"
    dcontext "MyString"
    channel "MyString"
    dstchannel "MyString"
    lastapp "MyString"
    lastdata "MyString"
    duration 1
    billsec 1
    disposition "MyString"
    amaflags 1
    accountcode "MyString"
    uniqueid "MyString"
    userfield "MyString"
    src_device_id 1
    dst_device_id 1
    processed 1
    did_price 1.5
    card_id 1
    provider_id 1
    provider_rate 1.5
    provider_billsec 1
    provider_price 1.5
    account_id "MyString"
    user_rate 1.5
    user_billsec 1
    user_price 1.5
    reseller_id 1
    reseller_rate 1.5
    reseller_billsec 1
    reseller_price 1.5
    partner_id 1
    partner_rate 1.5
    partner_billsec 1
    partner_price 1.5
    prefix "MyString"
    server_id 1
    hangupcause 1
    callertype "MyString"
    peerip "MyString"
    recvip "MyString"
    sipfrom "MyString"
    uri "MyString"
    useragent "MyString"
    peername "MyString"
    t38passthrough 1
    did_inc_price 1.5
    did_prov_price 1.5
    localized_dst "MyString"
    did_provider_id 1
    did_id 1
    originator_ip "MyString"
    terminator_ip "MyString"
    real_duration 1.5
    real_billsec 1.5
    did_billsec 1
    dst_user_id 1
  end
end
