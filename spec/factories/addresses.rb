# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    address_country "MyString"
    address_line_1 "MyString"
    address_line_2 "MyString"
    address_vat "MyString"
    address_phone_1 "MyString"
    address_phone_2 "MyString"
    address_fax "MyString"
  end
end
