# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :call_record do
    accounting_code 1
    user_name "MyString"
    call_date "2013-07-16 14:44:57"
    bill_sec 1
    user_price 1.5
    prefix "MyString"
  end
end
