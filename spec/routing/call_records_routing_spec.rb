require "spec_helper"

describe CallRecordsController do
  describe "routing" do

    it "routes to #index" do
      get("/call_records").should route_to("call_records#index")
    end

    it "routes to #new" do
      get("/call_records/new").should route_to("call_records#new")
    end

    it "routes to #show" do
      get("/call_records/1").should route_to("call_records#show", :id => "1")
    end

    it "routes to #edit" do
      get("/call_records/1/edit").should route_to("call_records#edit", :id => "1")
    end

    it "routes to #create" do
      post("/call_records").should route_to("call_records#create")
    end

    it "routes to #update" do
      put("/call_records/1").should route_to("call_records#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/call_records/1").should route_to("call_records#destroy", :id => "1")
    end

  end
end
