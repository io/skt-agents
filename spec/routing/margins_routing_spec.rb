require "spec_helper"

describe MarginsController do
  describe "routing" do

    it "routes to #index" do
      get("/margins").should route_to("margins#index")
    end

    it "routes to #new" do
      get("/margins/new").should route_to("margins#new")
    end

    it "routes to #show" do
      get("/margins/1").should route_to("margins#show", :id => "1")
    end

    it "routes to #edit" do
      get("/margins/1/edit").should route_to("margins#edit", :id => "1")
    end

    it "routes to #create" do
      post("/margins").should route_to("margins#create")
    end

    it "routes to #update" do
      put("/margins/1").should route_to("margins#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/margins/1").should route_to("margins#destroy", :id => "1")
    end

  end
end
