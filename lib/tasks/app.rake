begin

  namespace :app do
    
    namespace :assets do 
    
      task :reset => :environment do
        Rake::Task['assets:clean'].invoke
        Rake::Task['assets:precompile'].invoke        
      end
    
    end
    
    namespace :live do 
    
      task :install => :environment do
       ENV['UNICORN']=0
       Rake::Task['db:reset'].invoke
       Rake::Task['app:db:seed'].invoke
       Rake::Task['db:seed:agents'].invoke
      end
 
      task :restart => :environment do  
        sh 'service unicorn stop ' + ENV['APP_NAME']      
        sh '/usr/local/rvm/bin/skt_billing_unicorn_rails -c ' + Rails.root.to_s + '/config/unicorn.rb -D -E production'
      end        
    
      task :reset => :environment do  
        sh 'service unicorn stop ' + ENV['APP_NAME']
        Rake::Task['db:setup'].invoke
        Rake::Task['app:db:seed'].invoke 
        Rake::Task['db:seed:agents'].invoke
        sh 'unicorn_rails -c ' + Rails.root.to_s + '/config/unicorn.rb -D -E production'
      end  
    
    end
    
    task :uninstall => :environment do    
       sh 'script/uninstall.sh'
    end
    
    task :restart => :environment do
     ENV['UNICORN']=0
     sh 'script/backup.sh'   
     Rake::Task['app:db:reset'].invoke  
     sh 'rm -Rf tmp/cache/pids/*'
     sh 'rm -Rf tmp/cache/sockets/*'     
     sh 'truncate -s 0 log/unicorn*'     
    end
    
    task :reset => :environment do
     ENV['UNICORN']=0
     sh 'script/backup.sh'
     Rake::Task['app:db:reset'].invoke
     Rake::Task['app:assets:reset'].invoke
     sh 'rm -Rf tmp/cache/assets/*'
     sh 'rm -Rf tmp/cache/sass/*'
     sh 'rm -Rf tmp/cache/pids/*'
     sh 'rm -Rf tmp/cache/sockets/*'
     sh 'truncate -s 0 log/unicorn*'
    end
    
    task :install => :environment do
     Rake::Task['db:drop'].invoke
     Rake::Task['db:create'].invoke    
     sh 'script/pre_copy_app.sh'
     sh 'bundle install'  
     Rake::Task['app:db:create'].invoke
     sh 'script/install.sh'
     Rake::Task['db:drop'].invoke
     Rake::Task['db:create'].invoke
     Rake::Task['db:reset'].invoke
     Rake::Task['db:migrate'].invoke
     sh 'script/post_copy_app.sh'
     Rake::Task['app:db:seed'].invoke
     Rake::Task['db:seed:agents'].invoke
     Rake::Task['db:seed'].invoke        
     sh 'script/cleanup.sh'              
     sh 'script/backup.sh' 
    end
    

   
    task :startup => :environment do # not working      
      sh 'UNICORN=1 unicorn_rails -c /srv/skt_billing/config/unicorn.rb -D -E development'
    end
    
    task :backup => :environment do
      sh 'script/backup.sh'
    end

    task :cleanup => :environment do
      sh 'script/cleanup.sh'
    end        
    
    namespace :db do
    
      task :reset => :environment do
       Rake::Task['db:drop'].invoke
       Rake::Task['db:create'].invoke     
       Rake::Task['db:migrate'].invoke     
       Rake::Task['app:db:seed'].invoke
       Rake::Task['db:seed:agents'].invoke
       Rake::Task['db:seed'].invoke  
      end
    
      task :set_env => :environment do
        ENV['DB_NAME']=ENV['APP_NAME'] + '_' + Rails.env
      end
    
      task :pre_sync => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/pre_sync.sql'
      end
      
      task :post_sync => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/post_sync.sql'
      end      
      
      task :pre_setup => :environment do  
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/schema/pre_setup.sql'
      end
      
      task :post_setup => :environment do
        sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/schema/post_setup.sql'
      end         
    
      task :i_create => :environment do  
         ::SRC_TBL.each do |t|
           sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --no-data --no-create-db --skip-add-drop-table --skip-add-locks > db/schema/' + t[0] + '.sql'
           sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD" < db/schema/' + t[0] + '.sql'
          
         end         
      end
      
      task :i_seed => :environment do
        ::SRC_TBL.each do |t|
          sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --complete-insert --no-create-info --compact --where="' + t[1] + '" > db/seeds/' + t[0] + '.sql'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD" < db/seeds/' + t[0] + '.sql'
        end       	
      end
      
      task :i_empty => :environment do
        ::DST_TBL.each do |t|
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "TRUNCATE TABLE ' + t + '"'
        end      	
      end
      
      task :i_truncate_cdr => :environment do
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "create table _tmp(the_id int)"'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "insert into _tmp (select id from call_records where call_date > \'' + (Time.zone.now - 1.hour).strftime('%F %T') + '\')"'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "delete from call_records where not exists (select id from _tmp where the_id = id)"'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  -e "drop table _tmp"'
      end      
            
      task :i_sync => :environment do
        ::SYN_TBL.each do |t|
          sh 'mysqldump $REMOTE_DB_NAME ' + t[0] + ' -h $REMOTE_DB_HOST -u $REMOTE_DB_USR -p"$REMOTE_DB_PWD" --complete-insert --no-create-info --skip-add-drop-table --where="' + t[1] + '" > db/seeds/' + t[0] + '_sync.sql'
          sh 'mysql $DB_NAME -u $APP_DB_USR -p"$APP_DB_PWD"  < db/seeds/' + t[0] + '_sync.sql'
        end      	
      end
      
      task :test => [:set_env, :i_truncate_cdr]
      task :sync => [:set_env, :i_empty, :pre_sync, :i_sync, :post_sync, :i_truncate_cdr]
      task :seed => [:set_env, :pre_sync, :i_seed, :post_sync]
      task :create => [:set_env, :pre_setup, :i_create, :post_setup]
      
    end    
    
  end
  
end
